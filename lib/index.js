var path = require('path');

var Lng = function(copySettings) {
	this.reset();
	this.copySettings(copySettings);
	return this;
};

Lng.prototype.reset = function() {
	this.data = {};
	this.baseDir = null;
	this.projectKey = null;
	this.languageKey = null;
	this.useMetadataFormat = true;
};

Lng.prototype.copySettings = function(copySettings) {
	if('object'==typeof copySettings) {
		var copyKeys = ['baseDir', 'projectKey', 'languageKey', 'data', 'useMetadataFormat'];
		for(var ck in copyKeys) {
			if('undefined'!==typeof copySettings[copyKeys[ck]]) {
				this[copyKeys[ck]] = copySettings[copyKeys[ck]];
			}
		}
	}
};

Lng.prototype.load = function(options) {
	var opt = this.mergeObjects({
		languageKey: this.languageKey,
		projectKey: this.projectKey,
		data: undefined,
		forceUpdate: false
	}, options);
	if('undefined'===typeof this.data[opt.languageKey]) {
		this.data[opt.languageKey] = {};
	}
	if('object'==typeof opt.data) {
		this.data[opt.languageKey][opt.projectKey] = opt.data;
	} else {
		if('undefined'===typeof(this.data[opt.languageKey][opt.projectKey]) || options.forceUpdate) {
			var filePath = this.getFilePath(opt.languageKey, opt.projectKey);
			this.data[opt.languageKey][opt.projectKey] = this.getFileContents(filePath, options.forceUpdate);
		}
	}
	this.languageKey = opt.languageKey;
	this.projectKey = opt.projectKey;
	return this;
};

Lng.prototype.getFileContents = function(filePath, forceUpdate) {
	try {
		if(forceUpdate) {		
			var name = require.resolve(filePath);
			delete require.cache[name];
		}
		var data = require(filePath);
	} catch(e) {
		console.log('lng', 'getFileContents', filePath, 'e');
		var data = {};
	}
	return data;
};

Lng.prototype.getFilePath = function(languageKey, projectKey) {
	var fileName = projectKey+'-'+languageKey;
	var filePath = path.resolve(this.baseDir ? path.join(this.baseDir, fileName) : fileName);
	return filePath;
};

Lng.prototype.flattenArgs = function(args) {
	var a, arg, flat = [];
	for(i in args) {
		arg = args[i];
		if(typeof(arg)!='object') {
			flat.push(arg);
		} else {
			for(a in arg) {
				flat.push(arg[a]);
			}
		}
	}
	return flat;
};

Lng.prototype.mergeObjects = function(a, b) {
	if('object'!==typeof b) {
		return a;
	} else if('object'!==typeof a) {
		return b;
	} else {
		for(var i in b) {
			a[i] = b[i];
		}
		return a;
	}
};

Lng.prototype.get = function(key, options) {
	var opt = this.mergeObjects({
		type: 'text',
		languageKey: this.languageKey,
		projectKey: this.projectKey,
		stripTags: false,
		fallbackToDefault: true,
		parseLL: true
	}, options);
	key = key.toString().trim();
	if(key.indexOf(':')!==-1) {
		var parts = key.split(/:/);
		options.projectKey = parts[0];
		key = parts[1];
	}
	this.load({
		languageKey: opt.languageKey,
		projectKey: opt.projectKey
	});
	var val = null;
	if('undefined'!==typeof this.data[opt.languageKey][opt.projectKey][key]) {
		val = this.data[opt.languageKey][opt.projectKey][key]
		if(this.useMetadataFormat) {
			val = val.text;
		}
	}
	return val;
};

Lng.prototype.getData = function(options) {
	var opt = this.mergeObjects({
		languageKey: this.languageKey,
		projectKey: this.projectKey
	}, options);
	if('undefined'!==typeof(this.data[opt.languageKey]) && 'undefined'!==typeof(this.data[opt.languageKey][opt.projectKey])) {
		return this.data[opt.languageKey][opt.projectKey];
	} else {
		return false;
	}
}

module.exports = Lng;
