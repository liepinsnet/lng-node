var Lng = require('../');

var lng = new Lng({
	baseDir: __dirname,
	projectKey: 'example1',
	languageKey: 'en'
});

var testString = [
	'does.not.exist',
	'this.exists',
	'another.one',
];

for(var k in testString) {
	console.log(testString[k]+' =>', lng.get(testString[k]));
}
